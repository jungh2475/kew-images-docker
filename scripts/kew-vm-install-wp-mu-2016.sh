#!/bin/bash
#실행전 install-xxx.config.txt 수정 및 chmod +x 후 sudo로 실행하시오 

echo '#######################################'
echo '#                                     #'
echo '#  ubuntu(16.04)lamp-wordpress_mu     #'
echo '#    (mysql,php,plugins & themes)     #'
echo '#        by kewtea 20161213           #'
echo '#                                     #'
echo '#######################################'


########  0/3 check these variables first   ########

TASKNAME='kew-vm-install-wp-mu-2016'      ######### import! 중요, 이것이 파일명과 동일한가? unique한가?
CONFIGFILE=$TASKNAME-config.txt   
LOGFILE=$TASKNAME-log.txt
INSTALLSTEP="-1"
STARTTIME=$( date +'%s' )

echo '##### about this disk  #####'
echo 'disk overview using df -h'
df -h
#df -h | grep ^/dev #only showing pysical disks mounted
read -p "continue?" YESNO

#########  1/3  config file check, lgo(log), ctrl+c functions   ##############

if [ ! -f $CONFIGFILE ]
then
	echo 'please set up config file $CONFIGFILE first to start '
	exit
fi
echo loading... $CONFIGFILE 
source $CONFIGFILE

if [ ! -f "$LOGFILE" ]   # -f : regular file, -e file exists, -d directory, 
then
	echo new logfile created at $(date +'%Y-%m-%d %H:%M:%S') > $LOGFILE		
fi

#log function 
function lgo {
	if [ -z "$1" ]   # -n 혹은 다른 방식은 전체 인자 갯수 세기 - if [ $# -eq 0 ]
  	then
    	echo "No argument supplied"
    	return 0
	fi
	#lgo "iam happy" (example usage) 
	#이것은 매우 단순 버젼이고, backup-restore-test등 여러번 로그를 적는 lgo는 더 복잡/정교함 
	echo "$(date +'%Y-%m-%d %H:%M:%S'), $1" >> $LOGFILE  #실제로 로그 입력 부분
}

#ctrl+c exit function:trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
        echo "CTRL-C Exit at the stage of "$INSTALLSTEP
        lgo 'install stopped by ctrl-c exit at '$INSTALLSTEP
        ENDTIME=$( date +'%s')
        lgo 'elapsed time was '$(( $ENDTIME - $STARTTIME ))' seconds'  
	exit
}

##########  2/3  sub_install packages as function    #############

source ./test-install-spring-functions.sh
#STEP1="1/5: install (L)A(M)P" function install_apache_php {}
#STEP2="2/5: install mysql server" function install_mysql_server {}
#STEP3="3/5: install WP-MU" function install_wp_mu {}
#STEP4="4/5: install gcloud sdk" function install_gcloud {}
#STEP5="1/5: install etc" function install_etc {}
#########  3/3  main function        ###################

lgo "installation is starting at **************** " # $( date +'%Y/%m/%d-%H:%M:%S' )

if [ "$INSTALLSTEP" = "-1" ]
then
	INSTALLSTEP="$STEP1" #next step
fi
if [ "$INSTALLSTEP" = "$STEP1" ]
then
	echo '###### step 1/5: java & basic packages ##### '
	install_base_java #run function
	INSTALLSTEP="$STEP2" #next step
fi
if [ "$INSTALLSTEP" = "$STEP2" ]
then
	echo '###### step 2/5: springboots install #####'
	install_springboot #run function
	INSTALLSTEP="$STEP3" #next step
fi
if [ "$INSTALLSTEP" = "$STEP3" ]
then
	if [ -n "$INTSTALL_BYPASS_MYSQL" ]  # check for non-null/non-zero string variable or use -z
	then
		echo "install_mysql_bypass detected"
	else
		echo '###### step 3/5: mysql install #####'
		install_mysql_server #run function
	fi
	INSTALLSTEP="$STEP4" #next step
fi

if [ "$INSTALLSTEP" = "$STEP4" ]
then
	
	if [ -n "$INTSTALL_BYPASS_ELK" ]  # check for non-null/non-zero string variable or use -z
	then
		echo "install_elk_bypass detected"
	else	
		echo '###### step 4/5: elk install #####'
		install_elk #run function 
	fi
	INSTALLSTEP="$STEP5" #next step
fi

if [ "$INSTALLSTEP" = "$STEP5" ]
then
	#echo $INTSTALL_BYPASS_ELK
	echo '###### step 5/5: gcloud sdk gsutil install #####'
	install_gcloud_sdk #run function
	#INSTALLSTEP="$STEP6" #next step
fi



echo " all installation run completed well eof - good bye "
ENDTIME=$( date +'%s')
echo $ENDTIME
lgo 'total elapsed time : '$(( $ENDTIME - $STARTTIME ))' seconds'
