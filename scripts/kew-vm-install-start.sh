#!/bin/bash



INSTALLDIR="kew-install"  #설치할 로컬의 디렉토리 이름 
GITPATH="https://jungh2475@bitbucket.org/jungh2475/kew-images-docker.git"
INSTALLSHFILE="kew-vm-install-spring-mysql-elk-2016.sh"  #실행하고 싶은 파일 -루트로 가져와서 편집해서 실행한다 
INSTALLSHFILE2="kew-vm-tools-2016.sh"

cd ~/
mkdir $INSTALLDIR
cd $INSTALLDIR
git init
git remote add origin $GITPATH
git pull origin master
cd ~/
cp $INSTALLDIR'/scripts'$INSTALLSHFILE .
cp $INSTALLDIR'/scripts'$INSTALLSHFILE2 .
chmod +x *.sh
sudo nano $INSTALLSHFILE
#그리고 run : ./$INSTALLSHFILE
echo 'run : ./'$INSTALLSHFILE
echo 'good luck and bye bye'