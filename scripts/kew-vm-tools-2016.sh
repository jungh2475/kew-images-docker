#!/bin/bash
#실행전 kew-vm-tools-2016-config.txt 수정 및 chmod +x 후 sudo로 실행하시오

echo '#######################################'
echo '#                                     #'
echo '#    kewtea-virtualmachine(vm)-tools  #'
echo '#    ubuntu(16.04) backup,restore     #'
echo '#  test,restart,disk,backup,restore   #'
echo '#        by kewtea 20161115           #'
echo '#                                     #'
echo '#######################################'

########  0/3 check these variables first   ########

TASKNAME='kew-vm-tools-2016'  ######## import! 중요, 이것이 파일명과 동일한가? unique한가?
CONFIGFILE=$TASKNAME-config.txt   
LOGFILE=$TASKNAME-log.txt
LOGOLDFILE=$TASKNAME-logold.txt  #로그 파일이 일정크기 이상이 되면 마지막 것만 이리로 덮어씀. 
LOGFILESIZEMAX=1000  # kb 크기 이상이 되면 로그를 덮어 씀 
LOGCOUNT=0  #100번에 한번씩 파일 크기를 측정하기 위해, 100까지 세는 임시 변수 
TEMPFILE=$TASKNAME-temp.txt   #임시설정파일로, 삭제해도됨 
STARTTIME=$( date +'%s' )


#########  1/3  config+temp file check, lgo(log), ctrl+c functions   ##############
if [ ! -f $CONFIGFILE ]
then
	echo 'please set up config file $CONFIGFILE first to start '
	exit
fi
echo loading... $CONFIGFILE 
source $CONFIGFILE

if [ -f $TEMPFILE ]
then
	echo loading... $TEMPFILE
	source $TEMPFILE
fi

if [ ! -f "$LOGFILE" ]   # -f : regular file, -e file exists, -d directory, 
then
	echo new logfile created at $(date +'%Y-%m-%d %H:%M:%S') > $LOGFILE		
fi

#log function 
#lgo "iam happy"  # example 
function lgo {
	if [ -z "$1" ]   #-n 혹은 다른 방식은 전체 인자 갯수 세기 - if [ $# -eq 0 ]
  	then
    	echo "No argument supplied"
    	return 0
	fi
	#lgo "iam happy" (example usage) 
	#install-xxx.sh script에는 단순한 로그 기록기가 있음 
	if [ $LOGCOUNT -gt 100 ]
	then
		echo logcount 100 reached and checking log file size
		file_size=$( du -k $LOGFILE | awk '{print $1}' )  #du로 kbyte로 값을 얻어, awk로 첫$1값만 출력 
		if [ $file_size -gt $LOGFILESIZEMAX ];then
			echo logfile is full ...moving to old and create new one
			sudo mv $LOGFILE $LOGOLDFILE
			echo new logfile created at $(date +'%Y-%m-%d %H:%M:%S') > $LOGFILE # append면 >> 
		fi
		LOGCOUNT=0
	else 
		((LOGCOUNT=LOGCOUNT+1)) # or let "$LOGSAVECOUNT++"
		#echo log count up
	fi
	if [ ! -f "$LOGFILE" ]   # -f : regular file, -e file exists, -d directory,
	then
		echo new logfile created at $(date +'%Y-%m-%d %H:%M:%S') > $LOGFILE
	fi 
	echo "$(date +'%Y-%m-%d %H:%M:%S'), $1" >> $LOGFILE  #실제로 로그 입력 부분
}

#ctrl+c exit function:trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
        echo "CTRL-C Exit at the stage of "$SUBFUNCTION
        lgo "ctrl-c Exit at the stage of "$SUBFUNCTION
        ENDTIME=$( date +'%s')
        lgo 'elapsed time was '$(( $ENDTIME - $STARTTIME ))' seconds'  
	exit
}

lgo 'operation started'

#########  2/3  sub task functions   ##############

source test-tools-functions.sh
######  matching from test-tools-functions.sh #######
SUBFUNCTION="-1"
FUNCX="exit"   #설명에 주적을 달자 ....
FUNC1="backup_mysql_local"
FUNC2="backup_mysql_gstorage"
FUNC3="backup_elastic_local"
FUNC4="backup_elastic_gstorage"
FUNC5="backup_wp_local_full"
FUNC6="backup_wp_local_since"
FUNC7="backup_wp_gstorage"
FUNC8="restore_mysql_local"
FUNC9="restore_mysql_gstorage"
FUNC10="restore_elastic_local"
FUNC11="restore_elastic_gstorage"
FUNC12="restore_wp_local"
FUNC13="restore_wp_gstorage"
FUNC14="machine_details"
FUNC15="test_all"
FUNC16="restart_all"
FUNC17="google_storage_setup"
FUNC18="send_mail"

#########################

NumOfArgs=$#
FuncName=$1
echo 'detected numofArgs:'$NumOfArgs ' with '$FuncName

if [ ! -z $1 ]
then
	#if [ $1="test" ]
	
	ARG1=""
	if [ ! -z $2 ]  #if $2가 zero length이면 true, 즉 zero length가 아니니깐,....
	then
		ARG1=$2  #echo $ARG1
	fi
	
	sudo $FuncName $ARG1  #######이거 실행 되는지 확인해 보자 
fi







#################       3.3 select menu      #####################

echo 'project('$PROJECTID'),machine('$MACHINEID')'
echo 'never select gcloud if not installed gcloud sdk from google'
echo ''

PS3='Please enter your choice: ' #Prompt Screen 3 as select inside shell
options=($FUNC1 $FUNC2 $FUNC3 $FUNC4 $FUNC5 $FUNC6 $FUNCX)
select opt in "${options[@]}"
do
    case $opt in
        $FUNC1)
            #echo "you chose choice 1"
            SUBFUNCTION=$FUNC1
            backup_mysql_local
            ;;
        $FUNC2)
            #echo "you chose choice 2"
            SUBFUNCTION=$FUNC2
            backup_mysql_gstorage
            ;;
        $FUNC3)
            SUBFUNCTION=$FUNC3
            backup_elastic_local
            ;;
		$FUNC4)
	    	SUBFUNCTION=$FUNC4
	    	backup_elastic_gstorage
	    	;;
	    $FUNC5)
	    	SUBFUNCTION=$FUNC5
	    	backup_wp_local_full
	    	;;
	    $FUNC6)
	    	SUBFUNCTION=$FUNC6
	    	backup_wp_local_since
	    	;;
	    $FUNC7)
	    	SUBFUNCTION=$FUNC7
	    	backup_wp_gstorage
	    	;;
	    $FUNC8)
	    	SUBFUNCTION=$FUNC8
	    	restore_mysql_local
	    	;;
	    $FUNC9)
	    	SUBFUNCTION=$FUNC9
	    	restore_mysql_gstorage
	    	;;
	    $FUNC10)
	    	SUBFUNCTION=$FUNC10
	    	restore_elastic_local
	    	;;
	    $FUNC11)
	    	SUBFUNCTION=$FUNC11
	    	restore_elastic_gstorage
	    	;;
	    $FUNC12)
	    	SUBFUNCTION=$FUNC12
	    	restore_wp_local
	    	;;
	    $FUNC13)
	    	SUBFUNCTION=$FUNC13
	    	restore_wp_gstorage
	    	;;
	    $FUNC14)
	    	SUBFUNCTION=$FUNC14
	    	machine_details
	    	;;
	    $FUNC15)
	    	SUBFUNCTION=$FUNC15
	    	test_all
	    	;;
	    $FUNC16)
	    	SUBFUNCTION=$FUNC16
	    	restart_all
	    	;;
	    $FUNC17)
	    	SUBFUNCTION=$FUNC17
	    	google_storage_setup
	    	;;
	    $FUNC18)
	    	SUBFUNCTION=$FUNC18
	    	send_mail
	    	;;
        $FUNCX)
        	SUBFUNCTION=$FUNCX
        	exit_function
            break
            ;;
        *) echo invalid option;;
    esac
done


###################################################################
#lgo "$(date +'%Y-%m-%d %H:%M:%S'), iam happy"
lgo "iam happy....end of run"
ENDTIME=$( date +'%s')
lgo 'elapsed time was '$(( $ENDTIME - $STARTTIME ))' seconds'
echo "end of run"



