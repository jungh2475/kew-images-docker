#!/bin/bash

#1
function backup_mysql_local {  //full archiving
	MYSQLBACKUPFILE='kew_'$MYSQLDBNAME'_'$(date +%Y%m%d)'.sql'  #날짜별로 backup파일 생성
	echo 'backfup_file:'$MYSQLBACKUPFILE
	#sudo mysqldump --all-databases -u $MYSQLUSER -p$MYSQLPASSWORD > $BACKUP_DIR'/'$MYSQLBACKUPFILE
	sudo mysqldump -u $MYSQLUSER -p$MYSQLPASSWORD $MYSQLDBNAME > $BACKUP_DIR'/'$MYSQLBACKUPFILE
	#오래된 파일들은 삭제, 최근 하나만 남기고
	sudo find $BACKUPDIR -type f -name '*.sql' -mtime +1 -exec rm {} \;  #-delete가 더 좋은데 지원안되는 os있음 #last modified +more(-less) 1 day ago #-mnin시간이 지난 파일들은 다 삭제함 
	#혹시 크기로 찾고 싶다면 find . -size +386b -a -size -390b -exec rm -f {} \;	 
	#ls $BACKUPDIR | grep -P "^kew***.sql" | xargs -d"\n" rm
	
	 
}

#2
function backup_mysql_gstorage {
	#if [ -f $MYSQLBACKUPFILE ] then fi #if file exists
	gsutil cp $WP_DIR'/'$MYSQLBACKUPFILE $GS_BUCKET'/'$GS_MYSQLDIR
}

#3
function backup_elastic_local {
	//처음에 install하면서 snapshot 초기화가 되어져 있어야 한다 -확인해 보자
	curl -XPUT $EL_HOST'/_snapshot/'$EL_SNAPHOT'/_all'  #등록되어져 있으면 출력될것임
	EL_BACKUPNAME='snapshot_'$(date +'%Y%m%d') 
	#curl -XPOST로 ID를 지정하지 않고 넣어도 백업되지만 여기서는 날짜별로 명시적으로 저장한다 
	curl -XPUT $EL_HOST'/_snapshot/'$EL_SNAPHOT'/'$EL_BACKUPNAME'?wait_for_completion=true'
	#curl -XPUT $EL_HOST'/_snapshot/'$EL_SNAPHOT'/'$EL_BACKUPNAME -d '{ "indices": *, "ignore_unavailable": "true" }'
    #다되었으면 결과 확인 
    curl -XGET $EL_HOST'/_snapshot/'$EL_SNAPHOT'/'$EL_BACKUPNAME
    #예전것들은 삭제-https://www.karelbemelmans.com/2015/03/elasticsearch-backup-script-with-snapshot-rotation/
    #curl -XDELETE $EL_HOST'/_snapshot/'$EL_SNAPHOT'/'
    # Get a list of snapshots that we want to delete
	
	#json객체안의 snapshots에서 jq '.snapshots[] | .snapshot + " " + .end_time'....see https://gist.github.com/herrbuerger/10961577
	# Loop over the results and delete each snapshot
	DELETESNAPSHOTS=$( curl -s -XGET $EL_HOST'/_snapshot/'$EL_SNAPHOT'/_all' | jq -r ".snapshots[:-5][].snapshot" ) # 최근 5개만 보관 
	for DELETESNAPSHOT in $SNAPSHOTS
	do
 		echo 'Deleting snapshot: '$DELETESNAPSHOT
 		curl -s -XDELETE $EL_HOST'/_snapshot/'$EL_SNAPHOT'/'$DELETESNAPSHOT'?pretty'
	done
	echo 'old snapshot delete Done!'
    
}

#4
function backup_elastic_gstorage { #plugin으로 마치 local fs 사용하듯이 한다
	##########  https://www.elastic.co/guide/en/elasticsearch/plugins/master/repository-gcs.html
	##########  https://www.elastic.co/guide/en/elasticsearch/plugins/master/repository-gcs-usage.html
}

#5
function backup_wp_local_full {  //partial archiving, full archiving
	# $1  
	
	#case 1) full backup 
	WP_UPLOAD_BACKUPFILE=kew-wp-upload.$(date +'%Y%m%d-full').tar.gz  # 만들어질 파일 이름
	CMD1="sudo tar -zcvf $WP_UPLOAD_BACKUPFILE $WP_UPLOADDIR'/'"  #tar -zcvf archive-name.tar.gz directory-name 반대는 -zxvf, https://www.cyberciti.biz/faq/linux-unix-bsd-extract-targz-file/
	### 일정 CPU 점유율 이하로 유지
	sudo nice -n -10 $CMD1 #sudo nice -n -10 sudo -u $USER -c $CMD1 #-u myusername -c $CMD1
	#PSNAME='tar' #PID= $( ps aux | grep $PSNAME | awk '{print $2}' )
	#sudo renice -n 10 -p $PID  # existing process
	sudo mysqldump -u $WP_MYSQLUSER -p$WP_MYSQLPASSWORD $WP_DBNAME > $WP_MYSQL_BACKUPFILE
		
}

#6
function backup_wp_local_since {
	#case 2) diff backup
	BACKSPAN='-7'  #최근 7일간의 파일들만 백업 
	BACKFILES=$( find $BACKTARDIR -mtime $BACKSPAN )  # +7 older than 7 days(아주오래된 파일), -7 less than 7 
	WP_UPLOAD_BACKUPFILE=kew-wp-upload.$(date +'%Y%m%d-7').tar.gz  # 만들어질 파일 이름-만들어진 날짜, 기간7일간
	tar cvfz $WP_UPLOAD_BACKUPFILE $BACKFILES
	
}

#7
function backup_wp_gstorage {
	#gsutil cp
}

#8
function restore_mysql_local {
	mysql -u $MYSQLUSER -p$MYSQLPASSWORD < $MYSQLRESTOREFILE
}

#9
function restore_mysql_gstorage {
	#gsutil cp
}

#10
function restore_elastic_local {
	#1.먼져 잠시  _close
	curl -XPOST $EL_HOST'/_all/_close'# We need to close the index first _all or * : localhost:9200/my_index/_close
	#복구 snapshot 이름 정하기 
	if [ -z "$1" ]
	then
		EL_RESTORENAME=$1 #가장 최근 것을 찾던가 변수가 있으면 변수병을 대입한다
	else
		#가장 최근것 ....
	fi
	#2.전체 데이터 복구 -선택적인 복구도 가능함  -d' {  "indices": "index_1,index_2",
	curl -XPOST $EL_HOST'/_snapshot/'$EL_SNAPHOT'/'$EL_RESTORENAME'/_restore?pretty'
	# Restore the snapshot we want: curl -XPOST "http://localhost:9200/_snapshot/my_backup/$SNAPSHOT/_restore" -d '{ "indices": "my_index"}'
	#부분 restore: curl -XPOST '....snapshot_1/_restore?pretty' -d'{"indices": "index_1,index_2","ignore_unavailable": true,"include_global_state": true,"rename_pattern": "index_(.+)","rename_replacement": "restored_index_$1"}'
	#3. Re-open the index: 
	curl -XPOST $EL_HOST'/_all/_open'  #부분적인 복구를 위해서라면 -d '{ "indices": "my_index"...
	
	#4.복구 재대로 되었는지 확인 
	#current snapshot : curl -XGET "localhost:9200/_snapshot/my_backup/_current"...or delete using -XDELETE
	#curl -XGET 'localhost:9200/_snapshot/my_backup/_status?pretty'
	
}

#11
function restore_elastic_gstorage { #plugin으로 마치 local fs 사용하듯이 한다 
	
	
}

#12
function restore_wp_local {
	tar -zxvf $WP_UPLOAD_RESTOREFILE  -C $WP_UPLOADDIR  #extract
	mysql -u $MYSQLUSER -p$MYSQLPASSWORD < $WP_MYSQL_RESTOREFILE
}

#13
function restore_wp_gstorage {
	#gsutil cp
}

#14
function machine_details {
	#ip address
	
	HOSTIP=$( hostname --ip-address ) # not mac OSX,gcloud ubuntu에서는 이거면 됨
	# $(ifconfig eth0 | grep 'inet ' | cut -d: -f2 | awk '{ print $1}')
	#ifconfig |awk '/inet addr/{print substr($2,6)}' #ubuntu에서는 이거면 됨,내부2개가 보임  = hostname --ip-address 
	
	#if [ -n $HOSTIP ]
	#then
	#	HOSTIP=$( ifconfig en1 | grep 'inet ' | cut -d: -f2 | awk '{ print $2}' )  #
	#fi
	echo 'host_ip:'$HOSTIP
	# IP="$(ifconfig | grep -A 1 'eth0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
	#IP="$(ifconfig en1 | grep "inet" | tail -1 | cut -c7-19 )" # tested on macos
	IP="$( ifconfig en1 | grep "inet" | tail -1 | cut -f2 | cut -d' ' -f2 )" #tested on mac, inet 192.168.0.1
	echo $IP
	
	
	
	
	#running. stopped services list
	echo "***** list services running now *****"
	ps -ef | grep service  # to kill, sudo kill -9 process_number
	
	#disk usage
}

#15
function test_all {
	#(api testing?)
	# show packages details : apt-cache policy <package>, apt-show-versions <package>
	
	#ps(process) listing and find names
	
	
	#disk usage - quota left
	# sudo parted -l #get disk size status
	# echo "disk usage now:" # df -h
	
	#connection with gStorage
	
	#connection with elastic_gStorage
	
}

#16
function restart_all {
	
	#kill
	SEARCHWORD='[p]ython test.py'
	kill $(ps aux | grep $SEARCHWORD | awk '{print $2}')
	
	####### 0. check if this is ubuntu 16.04 higher
	OSNAME=$( python -mplatform )  # including version and 64bit
	OSUBUNTU=$( echo $OSNAME | grep -i 'ubuntu' )
	#OSNAMEVERSION=$( lsb_release -a | grep 'Description' | awk 'print $2}' )   #if os ubuntu 16+ 
	#cat /etc/version or python -mplatform | grep -qi 'Ubuntu' #quiet ignoreCase
	if [ -n $OSUBUNTU ]
	then
		echo "this script not support your os:"$OSNAME
		exit
	fi
	####### 1. kill(stop) the service to restart?
	####### 2. enable/disable/start/stop/restart/reload.....######
	#systemctl enable $1
	#systemctl enable elasticsearch.service
	#systemctl stop $1
	#systemctl list-units  --all (active+inactive)
	#systemctl list-units   (active)
	#systemctl -t myservice   #myservice 단어가 들어간 모든것?
	
	
	
}


#17
function google_storage_setup {
	#check disk size....
	#du (display usage) logging(log.csv) ls(list) rb(remove buckets) rm(remove fileObject)
	
	#for less than 5TB, use gsutil, otherwise use Cloud Storage Transfer Service : https://cloud.google.com/storage/transfer/

	#check if bucket(coldline) is created, if so, register here
	#use gsutil - install gCloud SDK & python (within in gce console, these are pre-installed)
	
	# 먼져 google console에서 gStorage를 통해 bucket을 생성하고, instance로 돌아와서 동작시킨다
	##### firt backup run case(create bucket) #####
	#gsutil mb gs://kewtea-storage-bucket-12/ #create bucket , 이방식 대신에 browser console 에서 만들자 
	#gsutil mb -c coldline -l us-east1 -p (project_id) gs://kewtea-storage-bucket-12/  #https://cloud.google.com/storage/docs/gsutil/commands/mb
	#console에서 folder들도 만들것 : /2016-11-05(생성 날짜) /mysql-elastic1, /elastic2, /tmp..
	#https://cloud.google.com/storage/docs/managing-buckets
}


#18
function send_mail {
	#gcloud outbound port 25 disabled
	#gcloud + ubuntu - kewtea's gmail relay : https://support.google.com/a/answer/2956491
	# admin.google.com > apps-gSuite-gamil-Advanced Settings > smtp relay service를 켠다 
	# smtp-relay.gmail.com port 25x, port 465, or port 587x.
	#http://www.johndball.com/configuring-sendmail-for-gmail-relay/
	#apt-get install sendmail mailutils sendmail-bin
	#nano /etc/mail/gmailauth
	#sudo sendmailconfig
	#/etc/mail/sendmail.mc file: define(`SMART_HOST', `smtp-relay.gmail.com')​​
	
	# ps -aux | grep sendmail
	# sendmail -t receiver@example 
	#echo -e "To: user@example.com\nSubject: Test\nTest\n" | sendmail -bm -t -v
	
	#ubuntu -postfix : https://easyengine.io/tutorials/linux/ubuntu-postfix-gmail-smtp/
	#if mac_osx
	CONTENT="Hello"  # $3
	SUBJECT="TEST MAIL"  # $1
	EMAILADDR="admin@kewtea.com"  # $2
	echo $CONTENT | mail -s $SUBJECT $EMAILADDR
	# if [ $# -lt 3 ] return or exit....else....echo $3 | mail -s $1 $2 
	
	#대신에 간단히 elasticsearch에 직접 post하는 방법도 있다: curl -XPOST "http://localhost:9200/indexname/typename/optionalUniqueId" -d "{ \"field\" : \"value\"}"  
	
}