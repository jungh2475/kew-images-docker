#!/bin/bash
project setting :
project-id: jungh-1131-1131
project : x-goog-project-id: 867067161628 (project number)
project-name: My Project 1131

console.cloud.google.com -> IAM & ADMIN에서 member 추가 
(Google Account email: user@gmail.com,Google Group: admins@googlegroups.com, Service account: server@example.gserviceaccount.com)
(개발자는 실험할때는 개인자격, 혹은 group으로 등록하고, production instance를 설정할때는 service account를 쓰면 좋겠음)  


https://console.cloud.google.com/storage/browser/kew-storage-jungh-1
GSBUCKET='gs://kew-storage-jungh-1'
GSLOGBUCKET='gs://kew-storage-jungh-1'
'gsutl cp '[source] '$GSBUCKET'/'$DESTFOLDER
folders: elastic-log-2016/, mysql-backups-2016/ wp-backups-2016/
where to create group? project access in bucket or gCloud project or userGroup or specific users?

cloud storage team id : 00b4903a971f3de7ef0e0e8a60740fb849c9252a3c04d2fb7c8514da22bd3156
gsutil mb $GSLOGBUCKET

# gStorage bucket size : 쉽게 알수 없음 : 아래식으로 각가그이 폴더를 돌면서 계산해 내야함 
# for DESTFOLDER in DESTFOLDERS
gsutil du -sh $GSBUCKET'/'$DESTFOLDER | cat > gstorage_size-$(date +'%Y%m%d')'.txt'
# 아니면 storage-analytics가 폴더안을 매일 들여다봐서 측정할수 있도록 하는 방법도 있음  
# -get daily log : https://cloud.google.com/storage/docs/access-logs#delivery
#gsutil acl ch -g cloud-storage-analytics@google.com:W $GSLOGBUCKET #grant the "cloud-storage-analytics@google.com" group write 
#gsutil logging set on -b $GSLOGBUCKET [-o kew_gstorage_log ] $GSBUCKET


GSTORELOGFILE='gstorage_size-'$(date +'%Y%m%d')'.txt'
g_size=0.0
echo 'measured at '$(date +'%d/%m/%Y %H:%M:%S') | cat > GSTORELOGFILE
for i in 'mysql-2016' 'elastic-2016' 'wp-2016' #DESTFOLDERS 여기에 sub폴더 이름들을 적는다 
do
   temp=$(gsutil du -sh $GSBUCKET'/'$DESTFOLDER)
   cat $temp >> $GSTORELOGFILE
   temp_floatnum=$(echo $temp | cut -d' ' -f 1 )
   g_size=$(echo $g_size+$temp_floatnum | bc )
done
echo $g_size >> $GSTORELOGFILE