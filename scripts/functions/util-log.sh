#!/bin/bash

#########  1/3  config file check, lgo(log), ctrl+c functions   ##############

#if [ ! -f $INSTALLSCRIPTDIR'/'$CONFIGFILE ]
#then
#	echo 'please set up config file '$INSTALLSCRIPTDIR'/'$CONFIGFILE' first to start '
#	exit
#fi
#echo loading... $CONFIGFILE 
#source $INSTALLSCRIPTDIR'/'$CONFIGFILE  # or $PROJECTDIR

if [ ! -f $PROJECTDIR'/'$LOGFILE ]   # -f : regular file, -e file exists, -d directory, 
then
	echo new logfile created at $(date +'%Y-%m-%d %H:%M:%S') > $PROJECTDIR'/'$LOGFILE		
fi

#log function 
function lgo {
	if [ -z "$1" ]   # -n 혹은 다른 방식은 전체 인자 갯수 세기 - if [ $# -eq 0 ]
  	then
    	echo "No argument supplied"
    	return 0
	fi
	#lgo "iam happy" (example usage) 
	#이것은 매우 단순 버젼이고, backup-restore-test등 여러번 로그를 적는 lgo는 더 복잡/정교함 
	echo "$(date +'%Y-%m-%d %H:%M:%S'), $1" >> $INSTALLSCRIPTDIR'/'$LOGFILE  #실제로 로그 입력 부분
}

#ctrl+c exit function:trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
        echo "CTRL-C Exit at the stage of "$INSTALLSTEP
        lgo "install stopped by ctrl-c exit at "$INSTALLSTEP
        ENDTIME=$( date +'%s')
        lgo "elapsed time was "$(( $ENDTIME - $STARTTIME ))" seconds"  
	exit
}