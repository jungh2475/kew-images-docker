#!/bin/bash

##########  2/3  sub_install packages as function    #############


STEP1="1/5: java and basic install"
function install_base_java {
	echo "running .....install_base_java"
	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get -y install python-software-properties
	sudo apt-get -y install software-properties-common
	sudo apt-get -y install maven
	sudo apt-get -y install git
	#install oracle java
	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install -y oracle-java8-installer
	sudo apt install oracle-java8-set-default
	#sudo apt-get -y install default-jre #sudo apt-get --yes install default-jre
	#sudo apt-get -y install default-jdk #sudo apt-get --yes install default-jdk 
	##set JAVA_HOME
	sudo update-alternatives --config java #list all java installations
	echo #####JAVA_HOME="/usr/lib/jvm/java-8-oracle/jre/bin/java" or "/usr/lib/jvm/java-8-oracle"
	echo 'LC_ALL="en_US.UTF-8"'
	readlink -e $(which java) #	readlink -f /usr/bin/java
	#ls -ln /usr/bin/java
	read -p "please edit /etc/environment to add JAVA_HOME, java on PATH(YESNO)" YESNO
	sudo nano /etc/environment
	source /etc/environment
	echo "checking java version and location"
	java -version
	which java
	echo 'path: '$PATH
	echo 'java_home: '$JAVA_HOME
	sudo apt-get -y install jq
	sudo apt-get -y install curl
	sudo apt install unzip
	read -p "finishing step1 ...now moving to step2 ...spring java install?(YESNO)" YESNO
}

STEP2="2/5: spring boot install"
function install_springboot {
	echo "running .....install_springboot"
	pwd
	echo "projectdir: "$PROJECTDIR  
	cd $PROJECTDIR
	echo "creating dir:" $SPRINGPROJECTDIR
	mkdir $SPRINGPROJECTDIR 
	cd $PROJECTDIR'/'$SPRINGPROJECTDIR
	echo "you are now at "
	pwd
	read -p "enter any to continue....(YESNO)" YESNO
	sudo git init
	sudo git remote add origin $SPRINGGIT
	sudo git pull origin master
	echo "don't forget to update /src/resources/application.properties with valid mysql details" 
	read -p "finishing step2 ...now moving to step3 ...mysql install?(YESNO)" YESNO
}

STEP3="3/5: mysql install"
function install_mysql_server {
	echo "running .....install_mysql_server at local for testing, and make sure you use remote origin db server..."
	sudo apt-get -y install mysql-server
	# mysql_secure_installation ; which removes the test database and secures the server for production.
	sudo mysql_secure_installation
	which mysql
	mysql --version
	echo "we have two mysqls -local and remote db server or is you the main db server?"
	read -p "test remote mysql connection with your account details?(Y)" YESNO
	if [ $YESNO = Y ] 
	then
		echo "you must explicitly allow access from this machine to main db server because secure_install did such DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
		echo "GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'$HOSTIPPRIVATE';"
		echo 'host:'$MYSQLHOSTIP ',db:'$MYSQLDBNAME ',u:'$MYSQLUSERNAME ',p:'$MYSQLUSERPASSWORD
		read -p "so we are connecting to remote(YESNO)" YESNO
		mysql -h $MYSQLREMOTEIP -D $MYSQLDBNAME -u $MYSQLUSERNAME -p$MYSQLUSERPASSWORD -e "use $MYSQLDBNAME; show tables; quit;"
		#remote mysql 서버가 현재 서버에서 접근하는 것을 허락하도록 해야한다
		read -p 'register user on remote mysql ......GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'$HOSTIPPRIVATE'; FLUSH PRIVILEGES;(YESNO)' YESNO
		sudo mysql -h $MYSQLREMOTEIP -u $MYSQLUSERNAME -p$MYSQLUSERPASSWORD
	else
		echo "bypass remote mysql server connection testing...."
	fi
	
	
	#mysql -h $MYSQLHOSTIP -D -u -p -e "quit;"	
	# mysql -h [ip] -D [db name] -u [user] -p[pass] -e "[mysql commands]"
	# mysql -u [user] -p[pass] << EOF
	#[mysql commands]
	#EOF
	
	 
	echo "starting local mysql server configuration..." #mysql -h '127.0.0.1' -D kew1db -u root -p1234 -e "use kew1db; show tables;"
	echo "create db, assign user...." #set localdb user with password (로컬에 테스트용 mysql에 할당 시키고, 리모트의 원래 mysql은 계정 정보 가져와서 알아서 적읍시다 )
	echo '###################################'	
	echo "mysql>CREATE DATABASE "$MYSQLDBNAME" CHARACTER SET utf8 COLLATE utf8_general_ci;"
	echo "mysql>CREATE USER \'"$MYSQLUSERNAME"\'@localhost IDENTIFIED BY \'"$MYSQLUSERPASSWORD"\';"
	echo "mysql>GRANT ALL PRIVILEGES ON * . * TO \'"$MYSQLUSERNAME"\'@localhost;"
	echo "FLUSH PRIVILEGES;"
	read -p "please login mysql as root and type above(YESNO)" YESNO
	sudo mysql -u root -p$MYSQLPASSWORD
	
	#sudo mysql -u root -p$MYSQLPASSWORD << EOF
	#CREATE DATABASE `$MYSQLDBNAME` CHARACTER SET utf8 COLLATE utf8_general_ci;
	#CREATE USER '$MYSQLUSERNAME'@'localhost' IDENTIFIED BY '$MYSQLUSERPASSWORD';
	#GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'localhost';
	#FLUSH PRIVILEGES;
	#EOF
	#set dbname with utf-8
	#GRANT [type of permission] ON [database name].[table name] TO ‘[username]’@'localhost’;
	#반대는 REVOKE,....사용자 삭제는 DROP USER ‘demo’@‘localhost’;,....
	
	
	TEMPSTR="mysql-local userName:"$MYSQLUSERNAME",password:"$MYSQLUSERPASSWORD
	echo $TEMPSTR
	lgo $TEMPSTR
	
	#restart mysql server
	sudo systemctl enable mysql   #d
	sudo systemctl start mysql   #d
	read -p "finishing step3 ...now moving to step4 ...elk(elastic) install?(YESNO)" YESNO
}

STEP4="4/5 elk install"
function install_elk {
	
	echo "installing filebeat, elasticsearch-kibana-logstash..."
	echo 'elk_install_path:'$ELK_INSTALL_PATH  # e.g. ~/elk
	read -p "if you want to change the path above, type here(path)" ELK_INSTALL_PATH
	if [ ! -d $ELK_INSTALL_PATH ]; then
  		mkdir -p $ELK_INSTALL_PATH;
	fi
	cd $ELK_INSTALL_PATH
	pwd
	
	read -p "select option A)filebeat only, B)ELK+filebeat (A/B)" AB
	if [ $AB = B ]
	then
		#1.install elastic
		wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.1.1.tar.gz
		tar -xzvf elasticsearch-5.1.1.tar.gz
		ls e*
		read -p "type elasticsearch bin directory name" ELASTIC_DIR
		#export PATH=$PATH:$ELK_INSTALL_PATH'/'$ELASTIC_DIR:
		#path.data: /var/data/elasticsearch, logs: /var/log/elasticsearch
		
		#2.install kibana
		wget https://artifacts.elastic.co/downloads/kibana/kibana-5.1.1-linux-x86_64.tar.gz
		tar -xzvf kibana-5.1.1-linux-x86_64.tar.gz
		ls k*
		read -p "type kibana directory name" KIBANA_DIR
		read -p "Set elasticsearch.url to point localhost:9200" YN
		sudo nano $ELK_INSTALL_PATH'/'$KIBANA_DIR'/config/kibana.yml'
		read -p "configure kibana-www.conf(enter)" YN
		sudo cp $INSTALLSCRIPTDIR'/scripts/elk_files/kibana-www.conf' $ELK_INSTALL_PATH'/'$KIBANA_DIR'/kibana-www.conf'
		sudo nano $ELK_INSTALL_PATH'/'$KIBANA_DIR'/kibana-www.conf'
		
		
		#3.install logstash
		wget https://artifacts.elastic.co/downloads/logstash/logstash-5.1.1.tar.gz
		tar -xzvf logstash-5.1.1.tar.gz
		ls l*
		read -p "type logstash directory name" LOGSTASH_DIR
		read -p "configure logstash-1.conf, logstash-2-apache.conf(enter)" YN
		sudo cp $INSTALLSCRIPTDIR'/scripts/elk_files/logstash-1.conf' $ELK_INSTALL_PATH'/'$LOGSTASH_DIR'/logstash-1.conf'
		sudo cp $INSTALLSCRIPTDIR'/scripts/elk_files/logstash-2-apache.conf' $ELK_INSTALL_PATH'/'$LOGSTASH_DIR'/logstash-2-apache.conf'
		logstash -f logstash.conf --config.test_and_exit
		sudo nano $ELK_INSTALL_PATH'/'$KIBANA_DIR'/kibana-www.conf'
		
		
		#한줄만 추가 할 경우 ... sudo echo $STR1 >> /etc/environment
		sudo cat >>/etc/environment <<EOF
		export PATH=$PATH:$ELK_INSTALL_PATH'/'$ELASTIC_DIR'/bin:'$ELK_INSTALL_PATH'/'$KIBANA_DIR'/bin:'$ELK_INSTALL_PATH'/'$LOGSTASH_DIR'/bin:'
		EOF 
		source /etc/environment
		
		#start & testing
		elasticsearch & #start elastic as daemon ; ./bin/elasticsearch -d -p pid
		curl http://localhost:9200/ #elastic
		kibana &
		echo "see kibana at localhost:5601"
		logstash -f logstash.conf &
		#그리고 부팅할때마다 자동으로 실행되도록.....
	else
	fi
	
	#4.install filebeat
	wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.1.1-linux-x86_64.tar.gz
	tar -xzvf filebeat-5.1.1-linux-x86_64.tar.gz
	ls f*
	echo 'export PATH='$PATH':'$ELK_INSTALL_PATH'/'$FILEBEAT_DIR':' >> /etc/environment
	source /etc/environment
	read -p "configure filebeat.yml?(Y/N)" YESNO
	sudo cp $INSTALLSCRIPTDIR'/scripts/elk_files/filebeat.yml' $ELK_INSTALL_PATH'/'$FILEBEAT_DIR'/filebeat.yml'
	sudo nano $ELK_INSTALL_PATH'/'$FILEBEAT_DIR'/filebeat.yml'
	sudo filebeat -e -c $ELK_INSTALL_PATH'/'$FILEBEAT_DIR'/filebeat.yml' & #start filebeat
	
	#####################  auto restart -enable service  #########################
	#install systemd-systemctl service and place at /etc/systemd/system/
	#https://linuxconfig.org/how-to-automatically-execute-shell-script-at-startup-boot-on-systemd-linux
	cp elk-autostart1.sh to ~/elk/elk-autostart.sh
	#sudo cat >>  <<EOF
	##!/bin/bash
	#export PATH=$PATH:
	#elasticsearch &
	#kibana &
	#logstash -f logstash.conf &
	#filebeat -e -c filebeat.yml &
	#EOF
	
	cp elk-start.service to /etc/systemd/system/
	chmod 744 ~/elk/elk-autostart.sh
	chmod 664 /etc/systemd/system/elk-start.service
	systemctl daemon-reload
	systemctl enable elk-start.service
	
		
	read -p "finishing step4 ...now moving to step5 ...gcloud sdk install?(YESNO)" YESNO
}



STEP5="5/5 gcloud sdk install"
function install_gcloud_sdk {
	#install python 2.7 : sudo apt-get install python2.7 python2.7-dev
	sudo apt-get install python #install python2 .....여기에 무언가 문제가 있음 ....
	python3 --version
	python --version
	
	
	#install gcloud sdk
	read -p "if this is within google cloud vm, you dont' need to install gsutil, it is already installed(enter)" YESNO
	
		#	export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"  ############ 여기도 문제가 있음 ....
		#echo "deb http://packages.cloud.google.com/apt "$CLOUD_SDK_REPO" main" | sudo tee /etc/apt/sources.list.d/google-cloud-sdk.list
		#curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
		#sudo apt-get update 
		#echo "now installing google-cloud-sdk......"	
		#sudo apt-get install -y google-cloud-sdk
		#sudo apt-get install google-cloud-sdk-app-engine-java
		#sudo gcloud init
		#Yes -login ...
		#gcloud components update
		#gsutil update
	
	read -p "we are now testing google storage connection ....(YESNO)" YESNO
	#test gcloud storage connection , 리모트에서 mysql_db가 백업/리스토어되므로, 여기서는 별로 백업할것이 없음, 그러나 연결만 테스트 해본다  
	#bucket을 만들고 싶으면, gsutil mb -c STANDARD -l US gs://[BUCKET_NAME]
	gsutil ls -l gs://$GCLOUDSTORAGE_BUCKET #see more at https://cloud.google.com/storage/docs/quickstart-gsutil
	
	
	#install gStorage plugin for elastic snapshot backup 
	sudo bin/elasticsearch-plugin install repository-gcs
	echo 'using PUT _snapshot/my_gcs_repository_on_compute_engine......{  "type": "gcs",  "settings": {  "bucket": "my_bucket",  "service_account": "_default_"}}'
	# see more on setting service account as file: https://www.elastic.co/guide/en/elasticsearch/plugins/master/repository-gcs-usage.html
	
	
	read -p "finishing step5 ...(now moving to step6?) ... close all?(YESNO)" YESNO
	echo $YESNO
}