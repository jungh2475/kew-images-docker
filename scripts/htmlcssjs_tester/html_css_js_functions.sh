
function single_file_merger(){
	# example: func 'a.txt b.txt c.txt’
	#[ $# -gt 1 ], 
	
	JSFILES=‘a.txt b.txt c.txt’
	if [ $1 ]
	then
		JSFILES=$1
	fi
	FILENAME=$PRJNAME’-‘$(date +”%Y%m%d”)
	$FILENAME.css
	
	cat $(echo $JSFILES) > $FILENAME.js

}