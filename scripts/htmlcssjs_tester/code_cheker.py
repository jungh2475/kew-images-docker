'''
'''

import re

class Code_Checker(object):
    def __init__(self):
        self.target=[]
        pass
    
    def load_script(self, scriptfilename):
        with open(scriptfilename, 'r') as myfile:    
            for line in myfile:
                #if 조건검
                self.target.append(line)  #한줄씩 적어서 넣음. 
            
            #self.target=myfile.readlines()
            #self.target=myfile.read().replace('\n', '') #scriptfilename
            #"".join(line.rstrip() for line in myfile)
        return len(self.target)
        
    def search_string(self, search_string):
        #result = re.match(pattern, string) = prog = re.compile(pattern), -> result = prog.match(string)
        #m = re.search(search_string) #'(?<=abc)def', 'abcdef')
        #m.group(0) #'def'
        result=re.match(search_string, self.target)
        return len(result)  #0이면 못찾은 것임. 2,3...등이 나오면 코멘트에 더 썻을 수도 잇음 

if __name__ == "__main__": 
    
    pass 