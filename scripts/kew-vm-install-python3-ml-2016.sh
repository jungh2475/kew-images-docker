#!/bin/bash
#실행전 install-xxx.config.txt 수정 및 chmod +x 후 sudo로 실행하시오 
# run by : ./install.sh or source ./install.sh

echo '#######################################'
echo '#                                     #'
echo '#  ubuntu(16.04)python3-mysql-elastic #'
echo '#        (pip,elk5,filebeat)          #'
echo '#        by kewtea 20161115           #'
echo '#                                     #'
echo '#######################################'


########  0/3 check these variables first   ########

TASKNAME='kew-vm-install-python3-ml-2016'      ######### import! 중요, 이것이 파일명과 동일한가? unique한가?
CONFIGFILE=$TASKNAME-config.txt   
LOGFILE=$TASKNAME-log.txt
INSTALLSTEP="-1"
STARTTIME=$( date +'%s' )

echo '##### about this disk  #####'
echo 'disk overview using df -h'
df -h
#df -h | grep ^/dev #only showing pysical disks mounted
read -p "continue?" YESNO

#########  1/3  config file check, lgo(log), ctrl+c functions   ##############

if [ ! -f $CONFIGFILE ]
then
	echo 'please set up config file $CONFIGFILE first to start '
	exit
fi
echo loading... $CONFIGFILE 
source $CONFIGFILE

if [ ! -f "$LOGFILE" ]   # -f : regular file, -e file exists, -d directory, 
then
	echo new logfile created at $(date +'%Y-%m-%d %H:%M:%S') > $LOGFILE		
fi

#log function 
function lgo {
	if [ -z "$1" ]   # -n 혹은 다른 방식은 전체 인자 갯수 세기 - if [ $# -eq 0 ]
  	then
    	echo "No argument supplied"
    	return 0
	fi
	#lgo "iam happy" (example usage) 
	#이것은 매우 단순 버젼이고, backup-restore-test등 여러번 로그를 적는 lgo는 더 복잡/정교함 
	echo "$(date +'%Y-%m-%d %H:%M:%S'), $1" >> $LOGFILE  #실제로 로그 입력 부분
}

#ctrl+c exit function:trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
        echo "CTRL-C Exit at the stage of "$INSTALLSTEP
        lgo 'install stopped by ctrl-c exit at '$INSTALLSTEP
        ENDTIME=$( date +'%s')
        lgo 'elapsed time was '$(( $ENDTIME - $STARTTIME ))' seconds'  
	exit
}

##########  2/3  sub_install packages as function    #############

source ./test-install-python-functions.sh
#STEP1="1/6  python3 and pip install" function install_python3_pip {}
#STEP1="2/6 python-ml-project-git install" function install_pygit_ml_project {}
#STEP1="3/6 mysql install" function install_mysql_server {}
#STEP1="4/6 elk install" function install_elk {}
#STEP1="5/6 gcloud sdk install" function install_gcloud_sdk {}
#STEP1="6/6 apache install" function install_apache {}

#########  3/3  main function        ###################

lgo "installation is starting at **************** " # $( date +'%Y/%m/%d-%H:%M:%S' )

if [ "$INSTALLSTEP" = "-1" ]
then
	INSTALLSTEP="$STEP1" #next step
fi
if [ "$INSTALLSTEP" = "$STEP1" ]
then
	echo '###### step 1/6  python3 and pip install  ##### '
	install_python3_pip #run function
	INSTALLSTEP="$STEP2" #next step
fi
if [ "$INSTALLSTEP" = "$STEP2" ]
then
	echo '###### step 2/6 python-ml-project-git install #####'
	install_pygit_ml_project #run function
	INSTALLSTEP="$STEP3" #next step
fi
if [ "$INSTALLSTEP" = "$STEP3" ]
then
	if [ -n "$INTSTALL_BYPASS_MYSQL" ]  # check for non-null/non-zero string variable or use -z
	then
		echo "install_mysql_bypass detected"
	else	
		echo '###### step 3/6 mysql install #####'
		install_mysql_server #run function
	fi
	INSTALLSTEP="$STEP4" #next step
fi

if [ "$INSTALLSTEP" = "$STEP4" ]
then
	
	if [ -n "$INTSTALL_BYPASS_ELK" ]  # check for non-null/non-zero string variable or use -z
	then
		echo "install_elk_bypass detected"
	else	
		echo '###### step 4/6 elk install #####'
		install_elk #run function 
	fi
	INSTALLSTEP="$STEP5" #next step
fi

if [ "$INSTALLSTEP" = "$STEP5" ]
then
	
	echo '###### step 5/6 gcloud sdk install #####'
	install_gcloud_sdk #run function
	INSTALLSTEP="$STEP6" #next step
fi

if [ "$INSTALLSTEP" = "$STEP6" ]
then
	
	echo '###### step 6/6 apache install #####'
	install_apache #run function
	#INSTALLSTEP="$STEP7" #next step
fi


echo " all installation run completed well eof - good bye "
ENDTIME=$( date +'%s')
echo $ENDTIME
lgo 'total elapsed time : '$(( $ENDTIME - $STARTTIME ))' seconds'
