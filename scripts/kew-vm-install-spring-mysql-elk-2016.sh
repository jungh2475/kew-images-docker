
#실행전 install-xxx.config.txt 수정 및 chmod +x 후 sudo로 실행하시오 

echo '#######################################'
echo '#                                     #'
echo '#  ubuntu(16.04)spring-mysql-elastic  #'
echo '#    (java8,maven,elk5,filebeat)      #'
echo '#        by kewtea 20161115           #'
echo '#                                     #'
echo '#######################################'


########  0/3 check these variables first   ########

HOSTIP_PUBLIC="10.240.0.3"  #218.153.100.86, 192.168.0.16
HOSTIP_PRIVATE="104.196.208.24"
CONFIGVERSION="1.0"

PROJECTDIR="/home/jungh_lee/webProjects"  #admin is developer's id   #current directory
TASKNAME='kew-vm-install-spring-mysql-elk-2016'      ######### import! 중요, 이것이 파일명과 동일한가? unique한가?
INSTALLSCRIPTDIR="/home/jungh_lee/webProjects/kew-install-images/scripts"
#INSTALLGIT="https://jungh2475@bitbucket.org/jungh2475/kew-images-docker.git"
CONFIGFILE=$TASKNAME-config.txt   
LOGFILE=$TASKNAME-log.txt
INSTALLSTEP="-1" # 중간부터 설치하고 싶으면 여기에 단계($INSTALLSTEP)를 적어서 jump 할수도 있다
#INTSTALL_BYPASS_ELK="1" #ELK(ElasticSearch) package를 설치하고 싶지 않은 경우는 대신에 이것을 on 시켜도 된다 
#INTSTALL_BYPASS_MYSQL="1" #MYSQL package를 설치하고 싶지 않은 경우 - 수동으로 직접 설치해 보세요
STARTTIME=$( date +'%s' )

###Spring Project & Spring Git

SPRINGPROJECTDIR="kew-spring-mysql"
SPRINGGIT="https://jungh2475@bitbucket.org/jungh2475/springboot_mysql.git"

#### mysql
MYSQLROOTPASSWORD="1234"
MYSQLUSERNAME="kewtea"  #추가하고 싶은 사용자명 (현재 db 및 원격 db둘가 적용 ) 
MYSQLUSERPASSWORD="1234"  # you must change this value
MYSQLREMOTEIP="192.168.0.23" # 원격 db서버 주소 
MYSQLDBNAME="kew1db"

#### elastic
ELK_INSTALL_DIR="" #설치할 디렉토리 
ELASTIC_SNAPHOT="kew_backup"
ELASTIC_FS_PATH="/home/jungh/backups/elastic_snapshots"
ELASTIC_INDEX=""
ELASTIC_LOG="" 
###########
ELASTIC_INSTALL_PATH="/usr/share/elasticsearch/"
ELASTIC_CONFIG="/etc/elasticsearch"
ELASTIC_HOST="http://localhost:9200"

KIBANA_HOME="/usr/share/kibana"
KIBANA_CONFIG="/etc/kibana"
LOGSTASH_INSTALL_PATH="/usr/share/logstash"
LOGSTASH_CONFIGS="/etc/logstash/conf.d"  
LOGSTASH_CONF1="logstash-1.conf"
LOGSTASH_LOG="/var/log/logstash"

#### filebeat
FILEBEAT=""

#### google storage
GCLOUDPROJECT_NAME="jungh-1131-1131"
GCLOUDPROJECT_NNUMBER="867067161628"
GCLOUDSTORAGE_BUCKET="kew-storage-jungh-1"
GCLOUDSTORAGE_MYSQL="mysql-backups-2016"
GCLOUDSTORAGE_ELASTIC="elastic-log-2016"
GCLOUDSTORAGE_WP="wp-backups-2016"
#### apache webserver
ServerName="kewtea.com"
ServerAlias="www.kewtea.com"
ServerAdmin="admin@kewtea.com"
SSLCertificateFile="/etc/apache2/ssl/default.crt"
SSLCertificateKeyFile="/etc/apache2/ssl/cherrytreewalk.com.key"
SSLCertificateChainFile="/etc/apache2/ssl/gd_bundle-g2-g1.crt"
ProxyHost="http://localhost:8080"






echo '##### about this disk  #####'
echo 'disk overview using df -h'
df -h
#df -h | grep ^/dev #only showing pysical disks mounted
read -p "continue?" YESNO

#########  1/3  config file check, lgo(log), ctrl+c functions   ##############
source $INSTALLSCRIPTDIR'/functions/util-log.sh'

##########  2/3  sub_install packages as function    #############

source $INSTALLSCRIPTDIR'/functions/test-install-spring-functions.sh'
#copy function names and STEPs
#STEP1="1/8: java and basic install" function install_base_java {}
#STEP2="2/8 spring boot install" function install_springboot {	echo "running .....install_springboot"}
#STEP3="3/8 mysql install" function install_mysql_server {	echo "running .....install_mysql_server"}
#STEP4="4/8 elk install"  function install_elk { echo "running .....install_elk(elasticsearch)" }
#STEP5="5/8 gcloud install" function install_gcloud { echo "running .....install_gcloud, gsutil" }

#########  3/3  main function        ###################

lgo "installation is starting at **************** " # $( date +'%Y/%m/%d-%H:%M:%S' )

if [ "$INSTALLSTEP" = "-1" ]
then
	INSTALLSTEP="$STEP1" #next step
fi
if [ "$INSTALLSTEP" = "$STEP1" ]
then
	echo '###### step 1/5: java & basic packages ##### '
	install_base_java #run function
	INSTALLSTEP="$STEP2" #next step
	lgo "installation 1 -java completed"
fi
if [ "$INSTALLSTEP" = "$STEP2" ]
then
	echo '###### step 2/5: springboots install #####'
	install_springboot #run function
	INSTALLSTEP="$STEP3" #next step
	lgo "installation 2 - springboot completed"
fi
if [ "$INSTALLSTEP" = "$STEP3" ]
then
	if [ -n "$INTSTALL_BYPASS_MYSQL" ]  # check for non-null/non-zero string variable or use -z
	then
		echo "install_mysql_bypass detected"
	else
		echo '###### step 3/5: mysql install #####'
		install_mysql_server #run function
		lgo "installation 3 - mysql completed"
	fi
	INSTALLSTEP="$STEP4" #next step
fi

if [ "$INSTALLSTEP" = "$STEP4" ]
then
	
	if [ -n "$INTSTALL_BYPASS_ELK" ]  # check for non-null/non-zero string variable or use -z
	then
		echo "install_elk_bypass detected"
	else	
		echo '###### step 4/5: elk install #####'
		install_elk #run function 
		lgo "installation 4 - elastic elk completed"
	fi
	INSTALLSTEP="$STEP5" #next step
fi

if [ "$INSTALLSTEP" = "$STEP5" ]
then
	#echo $INTSTALL_BYPASS_ELK
	echo '###### step 5/5: gcloud sdk gsutil install #####'
	install_gcloud_sdk #run function
	#INSTALLSTEP="$STEP6" #next step
	lgo "installation 5 - gcloud sdk completed"
fi



echo " all installation run completed well eof - good bye "
ENDTIME=$( date +'%s')
echo $ENDTIME
lgo "total elapsed time : "$(( $ENDTIME - $STARTTIME ))" seconds"
