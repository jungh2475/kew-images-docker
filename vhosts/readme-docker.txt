
[install]
------------
sudo apt-get install docker.io  (or sudo yum install-y docker)
[run on mac] docker-tool-box(mac/windows) with vbox
docker ports : ssh(22),http/s(80,443) + custom(49000-50000)
사용자 추가(add) sudo gpasswd -a userName docker 
ps docker

-------------
[commands]

docker --help
docker <command> --help
docker version
sudo service docker start
docker ps -a(all) 실행중이거나 전체 컨테이너 목록 출력
docker pull ubunutu:14.04 //이미지 저장 
docker images
docker build -t [imageName] ./apache/

docker commit //takes a snapshot of your container
docker commit -p  78727078a04b(container_id)  container1(snapshot_image_name_you_want)
docker images  //생겼는지 확인  
docker login
docker push container1
docker cp CONTAINER:PATH HOSTPATH
docker insert IMAGE URL PATH //insert a file into a Docker
사용하면서 생긴 파일(중간 저장본들은 volumeData에 따로 저장해 둔다)
add data volume disk
docker run -d -P --name web -v /webapp training/webapp python app.py

docker load -i /root/container1.tar  //restore

docker inspect [container_name]
docker run [image-local/cloud]  -d(background 실행) [imageName] [commands]
docker run abc/ddd echo "hello, I am jungh"  //즉시 종료 docker containers exit if there is no running application 
docker run -d centos sh /yourlocation/start.sh
docker run -d centos tail -f /dev/null
docker run -v /host/dir:container/dir www wordpress-img //외부 볼륨을 /var/www로 mount 
docker run --name wordpressdb -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=wordpress -d mysql:5.7
docker run -e WORDPRESS_DB_PASSWORD=password -d --name wordpress --link wordpressdb:mysql -p 127.0.0.2:8080:80 -v "$PWD/":/var/www/html  wordpress


docker stop :  컨테이너 종료 
docker rm [name]

docker log [container_id|name]
docker cp f9:/etc/passwd ./from_f9

-----------------
[target instances]

 python crawler
 python bot
 elasticsearch
 load_balancer
 vpn_proxy
 
---------------------
////https://www.sitepoint.com/how-to-use-the-official-docker-wordpress-image/
https://visible.vc/engineering/docker-environment-for-wordpress/

web:
    image: wordpress
    links:
     - mysql
    environment:
     - WORDPRESS_DB_PASSWORD=password
    ports:
     - "127.0.0.3:8080:80"
mysql:
    image: mysql:5.7
    environment:
     - MYSQL_ROOT_PASSWORD=password
     - MYSQL_DATABASE=wordpress


[docker compose]
	docker-compose.yml  => docker-compose up -d
		wordpress:
		  image: wordpress
		  links:
		    - wordpress_db:mysql
		  ports:
		    - 8080:80
		  volumes:
    		- ~/wordpress/wp_html:/var/www/html
		wordpress_db:
		  image: mariadb
		  environment:
		    MYSQL_ROOT_PASSWORD: examplepass
[wp-cli]
https://www.smashingmagazine.com/2015/09/wordpress-management-with-wp-cli/
	[install: wp-cli] 
		download via: curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
		php wp-cli.phar --info
		chmod +x wp-cli.phar
		sudo mv wp-cli.phar /usr/local/bin/wp  #path 경로로 가져다 놓음 

wp --info
wp cli update
wp core download
wp core download --version=4.2.2 --locale=el_GR
wp core update
wp core update-db
wp core version
wp core config --dbname=databasename --dbuser=databaseuser --dbpass=databasepassword --dbhost=localhost --dbprefix=prfx_
wp core multisite-install .......//https://wp-cli.org/commands/core/multisite-install/
wp core multisite-install --title="Welcome to the WordPress" --admin_user="admin" --admin_password="password" --admin_email="user@example.com"
wp core multisite-convert //- Transform a single-site install into a WordPress multisite install.
wp core install --url=example.com  --title="WordPress Website Title" --admin_user=admin_user --admin_password=admin_password --admin_email="admin@example.com"
wp core install --url="your_domain"  --title="Blog Title" --admin_user="admin username" --admin_password="enter_your_password" --admin_email="enter_your_email"
wp db export backup.sql
wp db query "SELECT id FROM wp_users;"
wp search-replace --dry-run 'dev.example.com' 'www.example.com'
wp theme install twentyten
wp theme activate p2
wp theme update twentyten
wp theme update --all
wp plugin install rest-api --activate
wp plugin activate woocommerce
wp plugin update woocommerce
wp plugin deactivate woocommerce
wp core update
wp super-admin list/add/remove  //이미 존재하는 사용자를 상대로 할수만  있음 
-------------------
cloud orchestration :docker swarm or kubernetics