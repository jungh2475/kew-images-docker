source documentation
and unit testing next to /src/module ....-> /test/[module]

#javaDoc
https://newcircle.com/bookshelf/java_fundamentals_tutorial/javadoc
/** */
@author name, @version major.minor.patch
@param name description, @return description, @throws Throwable description
@deprecated explanation,@see package.class#member label


javadoc -d /path/to/output /path/to/*.java
 
#jsDoc
/** @namespace */
@constructor, @class, @lends[namePath], @constructs, @extends
var   xxx
/**
         * Repeat <tt>str</tt> several times.
         * @param {string} str The string to repeat.
         * @param {number} [times=1] How many times to repeat the string.
         * @returns {string}
         * @throws {exceptionType} 
         */
@author, @version, @since  
@example
@see MyClass#myInstanceMethod

############### python  ########
#docString(__doc__) and pyDoc
파이썬은 설명을 위에 하지 않고, 바로 밑에 하게 된다 
docString: __doc__, 
class, def(함수) 키워드 바로 밑에 적음   
	'''
	'''
myObject.methodX.__doc__
pydoc module/className -> html
import pydoc -> help('moduleName')

pydoc sys(e.g. function, module, package) .Object(class,method, function)
pydoc -p 8080 -b (brwoser) -w out ./  =>out.html
python3/lib/pydoc ->pydoc module == import pydoc, help("module")
pydoc.HTMLDoc().docmodule(sys.modules["mymodule"])

python -m pydoc <function>  # -m: run library module as a script
python -c

doctest : >>>, docString """,,,,,"""
python example.py -v
[example.py].........
"""
>>> factorial(5)
120
"""
def factorial(n):

...

if __name__ == "__main__":
    import doctest
    #doctest.testfile("example.txt")
    doctest.testmod()
-------> 그것보다는 이런식으로  python -m doctest -v example.py
Note that this may not work correctly if the file is part of a package and imports other submodules from that package



""" ....module_name title  
description
parameters (inputs)
returns (output=yield)  #yield=one-time generator
exceptions

"""

pip install -U Sphinx (1.49) -> sphinx-quickstart (with conf.py)
sphinx-build -b html sourcedir builddir  #builder -b html

